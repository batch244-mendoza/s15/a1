	console.log ("Hello World ") ;


	// String Names

	let firstName = 'Arn';
	let name1 = "First Name: " +firstName;
	console.log(name1);

	let lastName = 'Mendoza'; 
	let name2 = "Last Name: " +lastName;
	console.log(name2);

	// Number 

	let age = 25;
	let age1 = "Age: " + age;
	console.log(age1);

	// Array 

	let hobbies1 = 'Hobbies: ';
	console.log(hobbies1);

	let hobbies = ["Hiking","Basketball","Playing Guitar"];
	console.log(hobbies);

	// Object

	let address = 'Address: ';
	console.log(address);


	let address1 = {
		houseNumber: "123",
		Street: "45",
		City: "Ormoc City",
		State: "Philippines",
	}

	console.log(address1);

// Debugging 

	// String Names

	let name = "Steve Rogers";
	console.log("My full name is" + name);
	
	// Number 

	let age3 = 40;
	console.log("My current age is: " + age3);
	
	// Array 

	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log("My Friends are: ")
	console.log(friends);

	// Object

	let profile = {

		username: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log("My Full Profile: ")
	console.log(profile);

	let fullName = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName);

	const lastLocation1 = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation1);	

	// End of the Activity s15